﻿CREATE TABLE [dbo].[Address]
(	
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[Street] [nvarchar](512) NOT NULL,
	[City] [nvarchar](512) NOT NULL,
	[Country] [nvarchar](512) NOT NULL
	CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [Person_Address_FK] FOREIGN KEY([PersonId]) 
	  REFERENCES [dbo].[Person] ([Id])
	  ON UPDATE CASCADE
	  ON DELETE CASCADE
)
