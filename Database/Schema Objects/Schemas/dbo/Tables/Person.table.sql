﻿CREATE TABLE [dbo].[Person]
(	
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](512) NULL,
	[Age] [int],
	CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED ([Id] ASC),
)
