﻿CREATE TABLE [dbo].[Phone]
(	
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PersonId] [int] NOT NULL,
	[PhoneNumber] [int] NOT NULL,
	CONSTRAINT [PK_Phone] PRIMARY KEY CLUSTERED ([Id] ASC),
	CONSTRAINT [Person_Phone_FK] FOREIGN KEY([PersonId]) 
	  REFERENCES [dbo].[Person] ([Id])
	  ON UPDATE CASCADE
	  ON DELETE CASCADE
)
