﻿namespace WebSharperProject
open IntelliFactory.WebSharper
open IntelliFactory.WebSharper.Html
open IntelliFactory.WebSharper.Formlet
open IntelliFactory.WebSharper.JQuery
open Microsoft.FSharp.Linq
open Microsoft.FSharp.Linq.Query
open System.Linq;
module PersonListDetails =
    module Server =
        [<Rpc>]
        let GetDetailData (id : int)  = 
            let db = new Model.WebSharperDemoDataContext()
            let addresses = Queryable.AsQueryable<Model.Address>(db.Addresses)
            let phoneNums = Queryable.AsQueryable<Model.Phone>(db.Phones)
            let emails = Queryable.AsQueryable<Model.Email>(db.Emails)
            let queryAddress = 
                query <@ seq { 
                    for a in addresses do 
                        if a.PersonId = id then 
                            yield ContactForms.AddressContact {
                                    Street = a.Street; 
                                    City = a.City;
                                    Country= a.Country }
                        } @>
            let queryPhone = 
                query <@ seq { 
                    for p in phoneNums do 
                        if p.PersonId = id then 
                            yield ContactForms.PhoneContact p.PhoneNumber 
                    } @>
            let queryEmail = 
                query <@ seq { 
                    for e in emails do 
                        if e.PersonId = id then 
                            yield ContactForms.EmailContact e.EmailAddress
                    } @>
            let q = queryAddress.Union(queryPhone).Union(queryEmail)
            q.FirstOrDefault()

    module Client = 
        [<JavaScript>]
        let VisibleClass = "Visible"
        
        [<Inline "jQuery($dom).hasClass($className)">]
        let hasClass (dom : Dom.Element, className : string ) : bool = 
            failwith "client"      

        [<JavaScript>]
        let TogglePanel (id: int, body : Element) : Element =
            let detail = Span []
            let content = LI [
                            body
                            detail
                            ]
            content
            |> OnClick (fun content event ->
                if hasClass(content.Dom, VisibleClass) then
                   detail.Clear()
                   JQuery.Of(content.Dom).RemoveClass(VisibleClass).Ignore
                else
                    let con = Server.GetDetailData (id) 
                    detail.Clear()
                    match con with
                    | ContactForms.AddressContact add -> 
                        detail.Append(
                            Div [
                                Div [B [Text "Street: "]; 
                                    Span [add.Street|> Text ]
                                ]
                                Div [
                                    B [Text "City: "]; 
                                    Span [add.City|> Text ]
                                ]
                                Div [
                                    B [Text "Country: "]; 
                                    Span [add.Country |> Text]
                                ]
                            ]
                        )
                    | ContactForms.PhoneContact pNum -> 
                        detail.Append(
                            Div [
                                Div [B [Text "Phone: "]; 
                                    Span [pNum.ToString() |> Text ]
                                ]
                            ]
                        )
                    | ContactForms.EmailContact email ->
                        detail.Append(
                            Div [
                                Div [B [Text "Email: "]; 
                                    Span [email |> Text ]
                                ]
                            ]
                        )
                    JQuery.Of(content.Dom).AddClass(VisibleClass).Ignore
                    JQuery.Of(detail.Dom).SlideDown("fast", ignore).Ignore
                ) |> ignore
            content