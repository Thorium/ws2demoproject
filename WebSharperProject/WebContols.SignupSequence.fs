﻿namespace WebSharperProject
open IntelliFactory.WebSharper
open IntelliFactory.WebSharper.Html

type PersonListControl() =
    inherit Web.Control()

    [<JavaScript>]
    override this.Body = PersonList.Client.Main() :> _

